package com.ubd.vo;

import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonProperty;

public class InstructionVO {
	Map<String, String> embedLinks;
	Map<String, String> images;
	Map<String, String> texts;
	Map<String, String> videos;
 
	public InstructionVO() {
	}

	public InstructionVO(Map<String, String> embedLinks, Map<String, String> images, Map<String, String> texts,
			Map<String, String> videos) {
		super();
		this.embedLinks = embedLinks;
		this.images = images;
		this.texts = texts;
		this.videos = videos;
	}

	@JsonProperty
	public Map<String, String> getEmbedLinks() {
		return embedLinks;
	}

	public void setEmbedLinks(Map<String, String> embedLinks) {
		this.embedLinks = embedLinks;
	}

	@JsonProperty
	public Map<String, String> getImages() {
		return images;
	}

	public void setImages(Map<String, String> images) {
		this.images = images;
	}

	@JsonProperty
	public Map<String, String> getTexts() {
		return texts;
	}

	public void setTexts(Map<String, String> texts) {
		this.texts = texts;
	}

	@JsonProperty
	public Map<String, String> getVideos() {
		return videos;
	}

	public void setVideos(Map<String, String> videos) {
		this.videos = videos;
	}

}
