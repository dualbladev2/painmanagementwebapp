package com.ubd.util;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.SerializerProvider;

public class CustomDatetimeSerializer extends JsonSerializer<Date> {
	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH-mm");

	@Override
	public void serialize(Date date, JsonGenerator jgen, SerializerProvider serializers)
			throws IOException, JsonProcessingException {
		String formatedDate = sdf.format(date);
		jgen.writeString(formatedDate);
	}

}
