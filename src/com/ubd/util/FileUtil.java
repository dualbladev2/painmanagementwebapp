package com.ubd.util;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

public class FileUtil {

	public static byte[] readImage(String path) throws IOException {
		File f = new File(path);
		BufferedImage bufferedImage = ImageIO.read(f);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(bufferedImage, "jpg", baos);
		byte[] imageData = Base64.getEncoder().encode(baos.toByteArray());
		return imageData;
	}

	public static String readImageBase64String(String path) throws IOException {
		File f = new File(path);
		BufferedImage bufferedImage = ImageIO.read(f);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(bufferedImage, "jpg", baos);
		String imageData = Base64.getEncoder().encodeToString(baos.toByteArray());
		return imageData;
	}

	public static boolean saveImage(String path, InputStream uploadedInputStream) throws IOException {
		File fileToUpload = new File(path);
		File folder = fileToUpload.getParentFile();
		if (folder != null) {
			if (!folder.exists()) {
				folder.mkdirs();
			}
		}
		BufferedImage bi = ImageIO.read(uploadedInputStream);
		return ImageIO.write(bi, "jpg", fileToUpload);
	}

	public static void removeFile(String path) throws IOException {
		Files.delete(Paths.get(path));
	}

	public static String readFileBase64String(String path) throws IOException {
		File f = new File(path);
		String imageData = Base64.getEncoder().encodeToString(Files.readAllBytes(f.toPath()));
		return imageData;
	}

	public static Map<String, String> readAllFilesInFolderToBase64(String path) throws IOException {
		final File folder = new File(path);
		Map<String, String> map = new HashMap<String, String>();
		for (final File fileEntry : folder.listFiles()) {
			if (!fileEntry.isDirectory()) {
				map.put(fileEntry.getName(), readFileBase64String(fileEntry.getAbsolutePath()));
			}
		}
		return map;
	}

	public static Map<String, String> readKeyValueTextFile(String textPath) throws IOException {
		Map<String, String> textMap = new HashMap<String, String>();
		File textFile = new File(textPath);
		List<String> lineList = Files.readAllLines(textFile.toPath());
		String[] temp;
		for (String line : lineList) {
			temp = line.split(":", 2);
			textMap.put(temp[0], temp[1]);
		}
		return textMap;
	}

	public static InputStream convertBase64ImageToInputStream(String base64Image) throws IOException {
		byte[] b = Base64.getDecoder().decode(base64Image);
		ByteArrayInputStream bais = new ByteArrayInputStream(b);
		return bais;
	}
}
