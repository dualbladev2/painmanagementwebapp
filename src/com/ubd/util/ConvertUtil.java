package com.ubd.util;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class ConvertUtil {
	public static String get_SHA_512_EnCode(String str) {
		String result = null;
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-512");
			byte[] bytes = md.digest(str.getBytes(StandardCharsets.UTF_8));
			BigInteger no = new BigInteger(1, bytes);
			result = no.toString(16);
			while (result.length() < 32) {
				result = "0" + result;
			}
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return result;
	}

}
