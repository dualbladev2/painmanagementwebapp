package com.ubd.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import io.jsonwebtoken.Jwts;

/**
 * Servlet Filter implementation class JWTTokenValidateFilter
 */
@WebFilter(filterName = "validateJWTTokenFilter")
public class ValidateJWTTokenFilter implements Filter {

	/**
	 * Default constructor.
	 */
	public ValidateJWTTokenFilter() {
		// TODO Auto-generated constructor stub

	}

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		// TODO Auto-generated method stub
		// place your code here

		String authorizationHeader = ((HttpServletRequest) request).getHeader(HttpHeaders.AUTHORIZATION);
		if (authorizationHeader == null || !authorizationHeader.startsWith("Bearer ")) {
			((HttpServletResponse) response).sendError(HttpServletResponse.SC_NOT_ACCEPTABLE);
		} else {
			String token = authorizationHeader.substring("Bearer".length()).trim();
			try {
				ServletContext context = ((HttpServletRequest) request).getServletContext();
				String secretKey = context.getInitParameter("secretKey");
				// Validate the token
				String icNumber = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody().get("sub")
						.toString();
				request.setAttribute("icNumber", icNumber);
			} catch (Exception e) {
				((HttpServletResponse) response).sendError(HttpServletResponse.SC_UNAUTHORIZED);
			}
		}
		// pass the request along the filter chain
		if (!response.isCommitted()) {
			chain.doFilter(request, response);
		}
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
