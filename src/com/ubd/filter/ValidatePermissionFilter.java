package com.ubd.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.HttpHeaders;

import io.jsonwebtoken.Jwts;

/**
 * Servlet Filter implementation class ValidatePermissionFilter
 */
@WebFilter(filterName = "validatePermissionFilter")
public class ValidatePermissionFilter implements Filter {

	/**
	 * Default constructor.
	 */
	public ValidatePermissionFilter() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	private static final String path = "/rest/JWTTokenNeeded/patient/";

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		// TODO Auto-generated method stub
		// place your code here
		try {
			String ctxPath = request.getServletContext().getContextPath();
			String pathParam = ((HttpServletRequest) request).getRequestURI().substring((ctxPath + path).length())
					.split("/")[0];
			String icNumber = (String) request.getAttribute("icNumber");
			if (!icNumber.equals(pathParam)) {
				throw new Exception();
			}
		} catch (Exception e) {
			((HttpServletResponse) response).sendError(HttpServletResponse.SC_UNAUTHORIZED);
		}

		// pass the request along the filter chain
		chain.doFilter(request, response);
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
