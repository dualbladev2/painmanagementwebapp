package com.ubd.api;

import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.ubd.dao.CustomerDao;
import com.ubd.dao.impl.CustomerDaoImpl;
import com.ubd.domain.Customer;

@Path("/customers")
public class CustomerService {

	private CustomerDao customerDao = CustomerDaoImpl.getInstance();

	@Context
	private ServletContext servletContext;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Customer> message() {

		List<Customer> customers = customerDao.findAllCustomers();
		return customers;
	}
}
