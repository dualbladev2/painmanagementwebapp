package com.ubd.api;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.ubd.util.FileUtil;
import com.ubd.vo.InstructionVO;

@Path("/instruction")
public class InstructionService {
	@Context
	private ServletContext servletContext;

	@GET
	@Path("/getInjectionInstruction")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getInjectionInstruction() throws JSONException {
		String path = servletContext.getRealPath(servletContext.getInitParameter("injectionInstructionPath"));
		
		String videoLinks = path + "/videoLinks";
		String videoPath = path + "/video";
		String imagePath = path + "/image";
		String textPath = path + "/text";
		
		InstructionVO vo = new InstructionVO();
		try {
			vo.setImages(FileUtil.readAllFilesInFolderToBase64(imagePath));
			vo.setVideos(FileUtil.readAllFilesInFolderToBase64(videoPath));
			vo.setTexts(FileUtil.readKeyValueTextFile(textPath));
			vo.setEmbedLinks(FileUtil.readKeyValueTextFile(videoLinks));
		} catch (IOException e) {
			e.printStackTrace();
		}
		JSONObject jo = new JSONObject();
		
		JSONArray jaTemp =null;
		JSONObject joTemp=null;
		
		jaTemp = new JSONArray();
		for (Map.Entry<String, String> entry : vo.getEmbedLinks().entrySet()) {
			joTemp = new JSONObject();
			joTemp.put(entry.getKey(), entry.getValue());
			jaTemp.put(joTemp);
		}
		jo.put("embedLinks", jaTemp);
		
		jaTemp = new JSONArray();
		for (Map.Entry<String, String> entry : vo.getImages().entrySet()) {
			joTemp = new JSONObject();
			joTemp.put(entry.getKey(), entry.getValue());
			jaTemp.put(joTemp);
		}
		jo.put("images", jaTemp);
		
		jaTemp = new JSONArray();
		for (Map.Entry<String, String> entry : vo.getTexts().entrySet()) {
			joTemp = new JSONObject();
			joTemp.put(entry.getKey(), entry.getValue());
			jaTemp.put(joTemp);
		}
		jo.put("texts", jaTemp);
		
		jaTemp = new JSONArray();
		for (Map.Entry<String, String> entry : vo.getVideos().entrySet()) {
			joTemp = new JSONObject();
			joTemp.put(entry.getKey(), entry.getValue());
			jaTemp.put(joTemp);
		}
		jo.put("videos", jaTemp);
		
		
		return Response.ok(jo).build();
	}
}
