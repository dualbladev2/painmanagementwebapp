package com.ubd.api;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.ubd.dao.MedicineDao;
import com.ubd.dao.impl.MedicineDaoImpl;
import com.ubd.domain.Medicine;
import com.ubd.domain.Medicinetype;
import com.ubd.util.FileUtil;

@Path("/medicine")
public class MedicineService {

	private MedicineDao medicineDaoImpl = MedicineDaoImpl.getInstance();

	@Context
	private ServletContext servletContext;

	@Path("/allMedicine")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllMedicine() {
		List<Medicine> list = medicineDaoImpl.getAllMedicine();
		return Response.ok(list).build();
	}

	@Path("/allMedicineImage")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllMedicineImage() throws IOException {
		try {
			JSONObject o = new JSONObject();

			List<Medicine> list = medicineDaoImpl.getAllMedicine();
			for (Medicine medicine : list) {
				String path = servletContext.getRealPath(medicine.getImageUri());
				String imageData = FileUtil.readImageBase64String(path);
				try {
					o.put(medicine.getMedicineId().toString(), imageData);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			return Response.ok(o).build();
		} catch (IOException e) {
			return Response.noContent().build();
		}
	}
}
