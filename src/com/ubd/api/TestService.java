package com.ubd.api;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.ubd.dao.CustomerDao;
import com.ubd.dao.impl.CustomerDaoImpl;
import com.ubd.domain.Customer;

@Path("/JWTTokenNeeded/customers")
public class TestService {
	
	private CustomerDao customerDao = CustomerDaoImpl.getInstance();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Customer> message() {

		List<Customer> customers = customerDao.findAllCustomers();
		return customers;
	}



}
