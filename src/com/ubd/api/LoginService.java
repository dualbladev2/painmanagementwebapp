package com.ubd.api;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.core.HttpHeaders;

import com.ubd.dao.PatientDao;
import com.ubd.dao.impl.PatientDaoImpl;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Path("/login")
public class LoginService {

	private PatientDao patientDao = PatientDaoImpl.getInstance();

	@Context
	private UriInfo uriInfo;
	@Context
	private ServletContext servletContext;

	@Path("/test")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response authenticateUserTest(@FormParam("icNumber") String icNumber,
			@FormParam("password") String password) {
		try {

			if (!(icNumber.equals("admin") && password.equals("admin"))) {
				throw new Exception();
			}
			String token = issueToken(icNumber);
			// Return the token on the response
			return Response.ok().header(HttpHeaders.AUTHORIZATION, "Bearer " + token).build();

		} catch (Exception e) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response authenticateUser(@FormParam("icNumber") String icNumber, @FormParam("password") String password) {
		try {

			// Authenticate the user using the credentials provided
			authenticate(icNumber, password);

			// Issue a token for the user
			String token = issueToken(icNumber);

			// Return the token on the response
			return Response.ok().header(HttpHeaders.AUTHORIZATION, "Bearer " + token).build();

		} catch (Exception e) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
	}

	private void authenticate(String icNumber, String password) throws Exception {
		if (patientDao.checkLogin(icNumber, password) == false) {
			throw new Exception("login fail");
		}
	}

	private String issueToken(String icNumber) {
		SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm
				.forName(servletContext.getInitParameter("signatureAlgorithm"));
		String secretKey = servletContext.getInitParameter("secretKey");
		String jwtToken = Jwts.builder().setSubject(icNumber).setIssuer(uriInfo.getAbsolutePath().toString())
				.setIssuedAt(new Date()).setExpiration(toDate(LocalDateTime.now().plusMonths(2)))
				.signWith(signatureAlgorithm, secretKey).compact();

		return jwtToken;
	}

	private Date toDate(LocalDateTime localDateTime) {
		return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
	}
}
