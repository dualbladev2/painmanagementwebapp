package com.ubd.api;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.ubd.domain.Todo;

@Path("/todo")
public class TodoService {
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Todo getTodo() {

		Todo todo = new Todo();
		todo.setSummary("Application JSON Todo Summary");
		todo.setDescription("Application JSON Todo Description");
		return todo;

	}
}
