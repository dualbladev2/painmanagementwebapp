package com.ubd.api;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileSystemException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.exception.ConstraintViolationException;

import com.sun.jersey.multipart.FormDataParam;
import com.ubd.dao.ExerciseDao;
import com.ubd.dao.MedicineDao;
import com.ubd.dao.PatientDao;
import com.ubd.dao.PatientglucoseDao;
import com.ubd.dao.impl.ExerciseDaoImpl;
import com.ubd.dao.impl.MedicineDaoImpl;
import com.ubd.dao.impl.PatientDaoImpl;
import com.ubd.dao.impl.PatientglucoseDaoImpl;
import com.ubd.domain.Exercisetype;
import com.ubd.domain.Medicine;
import com.ubd.domain.Patient;
import com.ubd.domain.Patientglucose;
import com.ubd.util.FileUtil;

@Path("/JWTTokenNeeded/patient")
public class PatientService {

	private PatientDao patientDao = PatientDaoImpl.getInstance();
	private ExerciseDao exerciseDao = ExerciseDaoImpl.getInstance();
	private PatientglucoseDao patientglucoseDao = PatientglucoseDaoImpl.getInstance();
	private MedicineDao medicineDao = MedicineDaoImpl.getInstance();

	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH-mm");

	@Context
	private ServletContext servletContext;

	@Path("{icNumber}/profile")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Patient getProfile(@PathParam("icNumber") String icNumber) {
		Patient p = patientDao.getPatientProfile(icNumber);
		return p;
	}
	@Path("{icNumber}/patientAppointmentDate")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Patient getPatientAppointmentDate(@PathParam("icNumber") String icNumber) {
		Patient p = patientDao.getPatientAppointmentDate(icNumber);
		return p;
	}
	@Path("{icNumber}/updateProfile")
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateProfile(@PathParam("icNumber") String icNumber, @FormParam("phoneNumber") String phoneNumber,
			@FormParam("relativePhoneNumber") String relativePhoneNumber, @FormParam("address") String address,
			@HeaderParam("updateDatetime") String updateDatetime) {

		if (updateDatetime == null) {
			return Response.status(Response.Status.NOT_ACCEPTABLE).build();
		}
		if (!patientDao.checkUpdatedData(icNumber, new Date(Long.parseLong(updateDatetime)))) {
			return Response.status(Response.Status.CONFLICT).build();
		}

		boolean check = true;
		Patient p = new Patient();
		List<String> list = new ArrayList<>();
		list.addAll(Arrays.asList(phoneNumber, relativePhoneNumber, address));
		for (String string : list) {
			if (string.trim().length() == 0) {
				check = false;
			}
		}
		p.setIcNumber(icNumber);
		p.setPhoneNumber(phoneNumber);
		p.setRelativePhoneNumber(relativePhoneNumber);
		p.setAddress(address);

		if (check) {
			if (!patientDao.updatePatientProfile(p)) {
				check = false;
			}
		}
		if (check) {
			return Response.ok().build();
		} else {
			return Response.status(Response.Status.NOT_ACCEPTABLE).build();
		}
	}

	@Path("{icNumber}/updateAppointmentDatetime")
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateAppointmentDatetime(@PathParam("icNumber") String icNumber,
			@FormParam("appointmentDate") String appointmentDate,
			@HeaderParam("updateDatetime") String updateDatetime) {
		if (updateDatetime == null) {
			return Response.status(Response.Status.NOT_ACCEPTABLE).build();
		}
		if (!patientDao.checkUpdatedData(icNumber, new Date(Long.parseLong(updateDatetime)))) {
			return Response.status(Response.Status.CONFLICT).build();
		}

		boolean check = true;
		Patient p = new Patient();
		p.setIcNumber(icNumber);
		try {
			p.setAppointmentDate(new Date(Long.parseLong(appointmentDate)));
		} catch (NumberFormatException e) {
			check = false;
		}
		if (check) {
			if (!patientDao.updatePatientAppointmentDatetime(p)) {
				check = false;
			}
		}
		if (check) {
			return Response.ok().build();
		} else {
			return Response.status(Response.Status.NOT_ACCEPTABLE).build();
		}
	}

	@Path("{icNumber}/profile/profileImage")
	@GET
	@Produces(MediaType.MULTIPART_FORM_DATA)
	public Response getProfileImage(@PathParam("icNumber") String icNumber) throws IOException {
		try {
			String path = servletContext.getRealPath(patientDao.getPatientImageUri(icNumber));
			byte[] imageData = FileUtil.readImage(path);
			return Response.ok(imageData).build();
		} catch (IOException e) {
			return Response.noContent().build();
		}
	}

	@Path("{icNumber}/profile/uploadProfileImage")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response uploadProfileImage(@PathParam("icNumber") String icNumber,
			@FormDataParam("data") InputStream uploadedInputStream) throws IOException {
		boolean isErr = false;

		try {
			String relativePath = servletContext.getInitParameter("patientProfileImage") + "/" + icNumber + "_"
					+ new Date().getTime();
			String realPath = servletContext.getRealPath(relativePath);
			if (FileUtil.saveImage(realPath, uploadedInputStream)) {
				String oldUri = patientDao.getPatientImageUri(icNumber);
				if (patientDao.updatePatientImageUri(icNumber, relativePath)) {
					FileUtil.removeFile(oldUri);
				} else {
					isErr = true;
				}
			}
		} catch (FileSystemException e) {
			// log can not remove
		} catch (IOException ex) {
			ex.printStackTrace();
			isErr = true;
		}
		if (isErr) {
			return Response.notModified().build();
		} else {
			return Response.ok().build();
		}
	}

	@Path("{icNumber}/profile/uploadBase64ProfileImage")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response uploadBase64ProfileImage(@PathParam("icNumber") String icNumber,
			@FormDataParam("data") String base64Image) throws IOException {
		boolean isErr = false;

		try {
			String relativePath = servletContext.getInitParameter("patientProfileImage") + "/" + icNumber + "_"
					+ new Date().getTime();
			String realPath = servletContext.getRealPath(relativePath);
			if (FileUtil.saveImage(realPath, FileUtil.convertBase64ImageToInputStream(base64Image))) {
				String oldUri = patientDao.getPatientImageUri(icNumber);
				if (patientDao.updatePatientImageUri(icNumber, relativePath)) {
					FileUtil.removeFile(oldUri);
				} else {
					isErr = true;
				}
			}
		} catch (FileSystemException e) {
			// log can not remove
		} catch (IOException ex) {
			ex.printStackTrace();
			isErr = true;
		}
		if (isErr) {
			return Response.notModified().build();
		} else {
			return Response.ok().build();
		}
	}

	@Path("{icNumber}/updateDatetime")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUpdateTimestamp(@PathParam("icNumber") String icNumber) {
		Patient p = new Patient();
		p.setUpdateDatetime(patientDao.getUpdateDatetime(icNumber));
		return Response.ok(p).build();
	}

	@Path("{icNumber}/exercise")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getExercise(@PathParam("icNumber") String icNumber) {
		Patient p = exerciseDao.getExercise(icNumber);
		p.getExercisetype().getExercises().forEach(ex -> {
			try {
				String path = servletContext.getRealPath(ex.getImageUri());
				if (path != null) {
					if (path.length() != 0) {
						ex.setBase64Image(FileUtil.readImageBase64String(path));
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
		return Response.ok(p).build();
	}

	@Path("{icNumber}/inputPatientglucose")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Response inputPatientglucose(@PathParam("icNumber") String icNumber,
			@FormParam("bloodGlucose") Integer value) {
		Date insertDate = patientglucoseDao.addPatientglucose(icNumber, value);
		JSONObject o = new JSONObject();
		try {
			o.put("insertDate", insertDate.getTime());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return Response.ok(o).build();
	}

	@Path("{icNumber}/medicine")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMedicine(@PathParam("icNumber") String icNumber) {
		List<Medicine> list = medicineDao.getMedicine(icNumber);
		return Response.ok(list).build();
	}

	@Path("{icNumber}/addNewPatientMedicine")
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public Response addNewPatientMedicine(@PathParam("icNumber") String icNumber,
			@FormParam("medicineId") String medicineId) {
		try {
			if (medicineDao.addNewPatientMedicine(icNumber, Integer.parseInt(medicineId))) {
			}
		} catch (ConstraintViolationException e) {
			if (e.getCause().getMessage().toLowerCase().contains("duplicate")) {
				return Response.notModified().build();
			}
		}
		return Response.ok().build();
	}

	@Path("{icNumber}/removePatientMedicine")
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public Response removePatientMedicine(@PathParam("icNumber") String icNumber,
			@FormParam("medicineId") String medicineId) {
		if (medicineDao.removePatientMedicine(icNumber, Integer.parseInt(medicineId))) {
		}
		return Response.ok().build();
	}

	@Path("{icNumber}/patientGlucose")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPatientGlucose(@PathParam("icNumber") String icNumber) {
		List<Patientglucose> list = patientglucoseDao.getTop5PatientGlucose(icNumber);
		return Response.ok(list).build();
	}
}
