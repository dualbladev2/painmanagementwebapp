package com.ubd.api;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.ubd.dao.FoodDao;
import com.ubd.dao.impl.FoodDaoImpl;
import com.ubd.domain.Food;
import com.ubd.domain.Foodtype;
import com.ubd.util.FileUtil;

@Path("/food")
public class FoodService {
	private FoodDao foodDaoImpl = FoodDaoImpl.getInstance();

	@Context
	private ServletContext servletContext;

	@Path("/allFood")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllFood() {
		List<Foodtype> list = foodDaoImpl.getAllFood();
		return Response.ok(list).build();
	}

	@Path("/allFoodImage")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllFoodImage() throws IOException {
		try {
			JSONObject o = new JSONObject();
			List<Foodtype> list = foodDaoImpl.getAllFood();
			for (Foodtype foodtype : list) {
				for (Food food : foodtype.getFoods()) {
					String path = servletContext.getRealPath(food.getImageUri());
					String imageData = FileUtil.readImageBase64String(path);
					try {
						o.put(food.getFoodId().toString(), imageData);
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
			}
			return Response.ok(o).build();
		} catch (IOException e) {
			return Response.noContent().build();
		}
	}
}
