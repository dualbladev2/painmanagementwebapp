package com.ubd.dao;

import java.util.List;

import com.ubd.domain.Medicine;
import com.ubd.domain.Medicinetype;

public interface MedicineDao {
	public List<Medicine> getAllMedicine();

	public List<Medicine> getMedicine(String icNumber);

	public boolean addNewPatientMedicine(String icNumber, Integer medicineId);

	public boolean removePatientMedicine(String icNumber, Integer medicineId);
}
