package com.ubd.dao;

import java.util.List;

import com.ubd.domain.Foodtype;

public interface FoodDao {
	public List<Foodtype> getAllFood();
}
