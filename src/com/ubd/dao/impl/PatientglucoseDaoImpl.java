package com.ubd.dao.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.ubd.dao.PatientglucoseDao;
import com.ubd.domain.Patient;
import com.ubd.domain.Patientglucose;
import com.ubd.util.ConvertUtil;
import com.ubd.util.HibernateUtil;

public class PatientglucoseDaoImpl implements PatientglucoseDao {

	private static PatientglucoseDaoImpl patientglucoseDaoImpl = null;

	private SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

	public static PatientglucoseDaoImpl getInstance() {
		if (patientglucoseDaoImpl == null)
			patientglucoseDaoImpl = new PatientglucoseDaoImpl();
		return patientglucoseDaoImpl;
	}

	@Override
	public Date addPatientglucose(String icNumber, int value) {
		boolean check = false;
		Date insertDate = new Date();
		Session session = this.sessionFactory.openSession();
		try {
			Patientglucose pg = new Patientglucose();
			pg.setGlucoseValue(value);
			pg.setInsertTime(insertDate);
			pg.setPatient(PatientDaoImpl.getPatientByIcNumber(session, icNumber));
			session.save(pg);
			check = true;
		} finally {
			session.close();
		}
		if (!check) {
			insertDate = null;
		}
		return insertDate;
	}

	public List<Patientglucose> getTop5PatientGlucose(String icNumber) {
		List<Patientglucose> list = null;
		Session session = this.sessionFactory.openSession();
		try {
			Query query = session.createQuery(
					"from Patientglucose pg where pg.patient.icNumber=:icNumber order by pg.insertTime desc");
			query.setString("icNumber", icNumber);
			list = query.setMaxResults(5).list();
		} finally {
			session.close();
		}
		return list;
	}

}
