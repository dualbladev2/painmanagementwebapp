package com.ubd.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.ResultTransformer;
import org.hibernate.transform.Transformers;

import com.ubd.dao.MedicineDao;
import com.ubd.domain.Medicine;
import com.ubd.domain.Medicinetype;
import com.ubd.domain.Patient;
import com.ubd.domain.Patientmedicine;
import com.ubd.util.ConvertUtil;
import com.ubd.util.HibernateUtil;

public class MedicineDaoImpl implements MedicineDao {
	private static MedicineDaoImpl medicineDaoImpl = null;

	private SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

	public static MedicineDao getInstance() {
		if (medicineDaoImpl == null)
			medicineDaoImpl = new MedicineDaoImpl();
		return medicineDaoImpl;
	}

	@SuppressWarnings("unchecked")
	public List<Medicine> getAllMedicine() {
		List<Medicine> result = null;
		Session session = this.sessionFactory.openSession();
		try {
			Query q = session.createQuery("from Medicine");
			result = q.list();
		} finally {
			session.close();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<Medicine> getMedicine(String icNumber) {
		List<Medicine> result = null;
		Session session = this.sessionFactory.openSession();
		try {
			Query q = session.createQuery("select pm.medicine from Patientmedicine pm");
			result = q.list();
		} finally {
			session.close();
		}
		return result;
	}

	public boolean addNewPatientMedicine(String icNumber, Integer medicineId) {
		boolean check = false;
		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try {
			Patientmedicine pm = new Patientmedicine();
			pm.setPatient(PatientDaoImpl.getPatientByIcNumber(session, icNumber));
			pm.setMedicine(session.get(Medicine.class, medicineId));
			session.save(pm);
			check = true;
		} finally {
			session.getTransaction().commit();
			session.close();
		}
		return check;
	}

	public boolean removePatientMedicine(String icNumber, Integer medicineId) {
		boolean check = false;
		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try {
			Patient p = PatientDaoImpl.getPatientByIcNumber(session, icNumber);
			Query query = session.createQuery(
					"delete from Patientmedicine where patientId = :patientId and medicineId = :medicineId");
			query.setInteger("patientId", p.getPatientId());
			query.setInteger("medicineId", medicineId);
			check = query.executeUpdate() == 1;
		} finally {
			session.getTransaction().commit();
			session.close();
		}
		return check;
	}
}
