package com.ubd.dao.impl;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.ubd.dao.FoodDao;
import com.ubd.domain.Foodtype;
import com.ubd.util.HibernateUtil;

public class FoodDaoImpl implements FoodDao {
	private static FoodDaoImpl foodDaoImpl = null;

	private SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

	public static FoodDao getInstance() {
		if (foodDaoImpl == null)
			foodDaoImpl = new FoodDaoImpl();
		return foodDaoImpl;
	}

	@Override
	public List<Foodtype> getAllFood() {
		List<Foodtype> result = null;
		Session session = this.sessionFactory.openSession();
		try {
			Query q = session
					.createQuery("from Foodtype");
			result = q.list();
		} finally {
			session.close();
		}
		return result;
	}

}
