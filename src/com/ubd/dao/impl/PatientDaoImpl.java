package com.ubd.dao.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;

import com.ubd.dao.PatientDao;
import com.ubd.domain.Patient;
import com.ubd.domain.Patientglucose;
import com.ubd.util.ConvertUtil;
import com.ubd.util.HibernateUtil;

public class PatientDaoImpl implements PatientDao {

	private static PatientDaoImpl patientDaoImpl = null;

	private SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

	public static PatientDao getInstance() {
		if (patientDaoImpl == null)
			patientDaoImpl = new PatientDaoImpl();
		return patientDaoImpl;
	}

	@SuppressWarnings({ "unchecked", "unused" })
	@Override
	public boolean checkLogin(String icNumber, String password) {
		boolean check = false;
		Session session = this.sessionFactory.openSession();
		try {
			Query query = session.createQuery(
					"select p.patientId from Patient p where icNumber = :icNumber and password = :password");
			query.setString("icNumber", icNumber);
			query.setString("password", ConvertUtil.get_SHA_512_EnCode(password));
			List<Integer> list = query.setMaxResults(1).list();
			for (Integer id : list) {
				check = true;
			}
		} finally {
			session.close();
		}
		return check;
	}

	@SuppressWarnings("unchecked")
	public Patient getPatientProfile(String icNumber) {
		Patient p = null;
		Session session = this.sessionFactory.openSession();
		try {

			Criteria cr = session.createCriteria(Patient.class);
			cr.setProjection(Projections.projectionList().add(Projections.property("name"), "name")
					.add(Projections.property("bloodtype"), "bloodtype")
					.add(Projections.property("diabetestype"), "diabetestype")
					.add(Projections.property("email"), "email").add(Projections.property("gender"), "gender")
					.add(Projections.property("birthday"), "birthday")
					.add(Projections.property("phoneNumber"), "phoneNumber")
					.add(Projections.property("relativePhoneNumber"), "relativePhoneNumber")
					.add(Projections.property("address"), "address")
					.add(Projections.property("updateDatetime"), "updateDatetime"))
					.setResultTransformer(Transformers.aliasToBean(Patient.class));
			cr.add(Restrictions.eq("icNumber", icNumber));

			List<Patient> list = cr.list();

			if (list.size() != 0) {
				p = list.get(0);
				p.setIcNumber(icNumber);
			}
		} finally {
			session.close();
		}
		return p;
	}

	@SuppressWarnings("unchecked")
	public Patient getPatientAppointmentDate(String icNumber) {
		Patient p = null;
		Session session = this.sessionFactory.openSession();
		try {

			Criteria cr = session.createCriteria(Patient.class);
			cr.setProjection(
					Projections.projectionList().add(Projections.property("appointmentDate"), "appointmentDate")
							.add(Projections.property("updateDatetime"), "updateDatetime"))
					.setResultTransformer(Transformers.aliasToBean(Patient.class));
			cr.add(Restrictions.eq("icNumber", icNumber));

			List<Patient> list = cr.list();

			if (list.size() != 0) {
				p = list.get(0);
				p.setIcNumber(icNumber);
			}
		} finally {
			session.close();
		}
		return p;
	}

	@Override
	public boolean updatePatientProfile(Patient p) {
		boolean check = false;

		Session session = this.sessionFactory.openSession();
		try {
			Transaction transaction = session.beginTransaction();
			Query query = session.createQuery(
					"update Patient set phoneNumber=:phoneNumber, relativePhoneNumber=:relativePhoneNumber,"
							+ " address=:address, updateDatetime=:updateDatetime" + " where icNumber = :icNumber");
			query.setString("icNumber", p.getIcNumber());
			query.setString("phoneNumber", p.getPhoneNumber());
			query.setString("relativePhoneNumber", p.getRelativePhoneNumber());
			query.setString("address", p.getAddress());
			query.setTimestamp("updateDatetime", new Date());
			check = query.executeUpdate() == 1;
			transaction.commit();
		} finally {
			session.close();
		}

		return check;
	}

	@Override
	public boolean updatePatientAppointmentDatetime(Patient p) {
		boolean check = false;

		Session session = this.sessionFactory.openSession();
		try {
			Transaction transaction = session.beginTransaction();
			Query query = session
					.createQuery("update Patient set appointmentDate=:appointmentDate, updateDatetime=:updateDatetime"
							+ " where icNumber = :icNumber");
			query.setString("icNumber", p.getIcNumber());
			query.setTimestamp("appointmentDate", p.getAppointmentDate());
			query.setTimestamp("updateDatetime", new Date());
			check = query.executeUpdate() == 1;
			transaction.commit();
		} finally {
			session.close();
		}

		return check;
	}

	@SuppressWarnings("unchecked")
	public Date getUpdateDatetime(String icNumber) {
		Date result = null;
		Session session = this.sessionFactory.openSession();
		try {
			Criteria cr = session.createCriteria(Patient.class);
			cr.setProjection(Projections.projectionList().add(Projections.property("updateDatetime"), "updateDatetime"))
					.setResultTransformer(Transformers.aliasToBean(Patient.class));
			cr.add(Restrictions.eq("icNumber", icNumber));

			List<Patient> list = cr.list();
			if (list.size() != 0) {
				result = list.get(0).getUpdateDatetime();
			}
		} finally {
			session.close();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public String getPatientImageUri(String icNumber) {
		String result = null;
		Session session = this.sessionFactory.openSession();
		try {
			Criteria cr = session.createCriteria(Patient.class);
			cr.setProjection(
					Projections.projectionList().add(Projections.property("profilePictureUri"), "profilePictureUri"))
					.setResultTransformer(Transformers.aliasToBean(Patient.class));
			cr.add(Restrictions.eq("icNumber", icNumber));

			List<Patient> list = cr.list();
			if (list.size() != 0) {
				result = list.get(0).getProfilePictureUri();
			}
		} finally {
			session.close();
		}
		return result;
	}

	@Override
	public boolean updatePatientImageUri(String icNumber, String relativePath) {
		boolean check = false;

		Session session = this.sessionFactory.openSession();
		try {
			Transaction transaction = session.beginTransaction();
			Query query = session.createQuery(
					"update Patient set profilePictureUri = :profilePictureUri, updateDatetime=:updateDatetime"
							+ " where icNumber = :icNumber");
			query.setString("icNumber", icNumber);
			query.setTimestamp("updateDatetime", new Date());
			query.setString("profilePictureUri", relativePath);
			check = query.executeUpdate() == 1;
			transaction.commit();
		} finally {
			session.close();
		}

		return check;
	}

// UPDATE `painmanagement`.`patient` SET `UpdateDatetime` =  STR_TO_DATE('12/22/2019 9:09:00 AM', '%c/%e/%Y %r') WHERE (`PatientId` = '1');
	public boolean checkUpdatedData(String icNumber, Date updateDatetime) {
		boolean check = false;
		Session session = this.sessionFactory.openSession();
		try {
			Query query = session.createQuery(
					"select p.patientId from Patient p where icNumber = :icNumber and updateDatetime = :updateDatetime");
			query.setString("icNumber", icNumber);
			query.setTimestamp("updateDatetime", updateDatetime);
			List<Integer> list = query.setMaxResults(1).list();
			for (Integer id : list) {
				check = true;
			}
		} finally {
			session.close();
		}
		return check;
	}

	public static Patient getPatientByIcNumber(Session session, String icNumber) {
		Patient p = null;
		Criteria cr = session.createCriteria(Patient.class);
		cr.setProjection(Projections.projectionList().add(Projections.property("patientId"), "patientId"))
				.setResultTransformer(Transformers.aliasToBean(Patient.class));
		cr.add(Restrictions.eq("icNumber", icNumber));
		List<Patient> list = cr.list();

		if (list.size() != 0) {
			p = list.get(0);
			p.setIcNumber(icNumber);
		}
		return p;
	}
}
