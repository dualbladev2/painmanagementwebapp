package com.ubd.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;

import com.ubd.dao.ExerciseDao;
import com.ubd.dao.PatientDao;
import com.ubd.domain.Patient;
import com.ubd.util.HibernateUtil;

public class ExerciseDaoImpl implements ExerciseDao {

	private static ExerciseDaoImpl exerciseDaoImpl = null;

	private SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

	public static ExerciseDao getInstance() {
		if (exerciseDaoImpl == null)
			exerciseDaoImpl = new ExerciseDaoImpl();
		return exerciseDaoImpl;
	}

	@SuppressWarnings("unchecked")
	public Patient getExercise(String icNumber) {
		Patient result = null;
		Session session = this.sessionFactory.openSession();
		try {
			Criteria cr = session.createCriteria(Patient.class);
			cr.setProjection(Projections.projectionList().add(Projections.property("exercisetype"), "exercisetype"))
					.setResultTransformer(Transformers.aliasToBean(Patient.class));
			cr.add(Restrictions.eq("icNumber", icNumber));

			List<Patient> list = cr.list();
			if (list.size() != 0) {
				result = list.get(0);
			}
		} finally {
			session.close();
		}
		return result;
	}

}
