package com.ubd.dao;

import com.ubd.domain.Patient;

public interface ExerciseDao {
	public Patient getExercise(String icNumber);
}
