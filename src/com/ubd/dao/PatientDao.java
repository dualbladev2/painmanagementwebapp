package com.ubd.dao;

import java.util.Date;

import com.ubd.domain.Patient;

public interface PatientDao {
	boolean checkLogin(String patientId, String password);

	public Patient getPatientProfile(String icNumber);

	public Patient getPatientAppointmentDate(String icNumber);

	public String getPatientImageUri(String icNumber);

	public boolean updatePatientImageUri(String icNumber, String relativePath);

	public boolean updatePatientProfile(Patient p);

	public boolean updatePatientAppointmentDatetime(Patient p);

	public boolean checkUpdatedData(String icNumber, Date updateDatetime);

	public Date getUpdateDatetime(String icNumber);

}
