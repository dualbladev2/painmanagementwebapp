package com.ubd.dao;

import java.util.Date;
import java.util.List;

import com.ubd.domain.Patientglucose;

public interface PatientglucoseDao {
	public Date addPatientglucose(String icNumber, int value);

	public List<Patientglucose> getTop5PatientGlucose(String icNumber);
}
