package com.ubd.domain;
// Generated Mar 21, 2019 2:14:44 PM by Hibernate Tools 5.1.10.Final

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Foodtype generated by hbm2java
 */
@Entity
@Table(name = "foodtype", catalog = "painmanagement")
public class Foodtype implements java.io.Serializable {

	private Integer foodTypeId;
	private String name;
	private Set<Food> foods = new HashSet<Food>(0);

	public Foodtype() {
	}

	public Foodtype(String name, Set<Food> foods) {
		this.name = name;
		this.foods = foods;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "FoodTypeId", unique = true, nullable = false)
	public Integer getFoodTypeId() {
		return this.foodTypeId;
	}

	public void setFoodTypeId(Integer foodTypeId) {
		this.foodTypeId = foodTypeId;
	}

	@Column(name = "Name", length = 45)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "foodtype")
	public Set<Food> getFoods() {
		return this.foods;
	}

	public void setFoods(Set<Food> foods) {
		this.foods = foods;
	}

}
