package com.ubd.domain;
// Generated Mar 21, 2019 2:14:44 PM by Hibernate Tools 5.1.10.Final

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;

/**
 * Food generated by hbm2java
 */
@Entity
@Table(name = "food", catalog = "painmanagement")
public class Food implements java.io.Serializable {

	private Integer foodId;
	private Foodtype foodtype;
	private String name;
	private String imageUri;
	private String description;

	public Food() {
	}

	public Food(Foodtype foodtype) {
		this.foodtype = foodtype;
	}

	public Food(Foodtype foodtype, String name, String imageUri, String description) {
		this.foodtype = foodtype;
		this.name = name;
		this.imageUri = imageUri;
		this.description = description;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "FoodId", unique = true, nullable = false)
	public Integer getFoodId() {
		return this.foodId;
	}

	public void setFoodId(Integer foodId) {
		this.foodId = foodId;
	}

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FoodTypeId", nullable = false)
	public Foodtype getFoodtype() {
		return this.foodtype;
	}

	public void setFoodtype(Foodtype foodtype) {
		this.foodtype = foodtype;
	}

	@Column(name = "Name", length = 45)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "ImageUri")
	public String getImageUri() {
		return this.imageUri;
	}

	public void setImageUri(String imageUri) {
		this.imageUri = imageUri;
	}

	@Column(name = "Description")
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
