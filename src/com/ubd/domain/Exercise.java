package com.ubd.domain;
// Generated Mar 21, 2019 2:14:44 PM by Hibernate Tools 5.1.10.Final

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

/**
 * Exercise generated by hbm2java
 */

@Entity
@Table(name = "exercise", catalog = "painmanagement")
public class Exercise implements java.io.Serializable {

	private Integer exerciseId;
	@JsonIgnore
	private Exercisetype exercisetype;
	private String name;
	private String description;
	private String imageUri;
	@JsonSerialize(include = Inclusion.NON_EMPTY)
	@Transient
	private String base64Image;

	public Exercise() {
	}

	public Exercise(Exercisetype exercisetype) {
		this.exercisetype = exercisetype;
	}

	public Exercise(Integer exerciseId, Exercisetype exercisetype, String name, String description, String imageUri) {
		super();
		this.exerciseId = exerciseId;
		this.exercisetype = exercisetype;
		this.name = name;
		this.description = description;
		this.imageUri = imageUri;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "ExerciseId", unique = true, nullable = false)
	public Integer getExerciseId() {
		return this.exerciseId;
	}

	public void setExerciseId(Integer exerciseId) {
		this.exerciseId = exerciseId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ExerciseTypeId", nullable = false)
	public Exercisetype getExercisetype() {
		return this.exercisetype;
	}

	public void setExercisetype(Exercisetype exercisetype) {
		this.exercisetype = exercisetype;
	}

	@Column(name = "Name", length = 45)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "Description", length = 45)
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "ImageUri", length = 255)
	public String getImageUri() {
		return imageUri;
	}

	public void setImageUri(String imageUri) {
		this.imageUri = imageUri;
	}

	@JsonSerialize(include = Inclusion.NON_EMPTY)
	@Transient
	public String getBase64Image() {
		return base64Image;
	}

	public void setBase64Image(String base64Image) {
		this.base64Image = base64Image;
	}

}
