package com.ubd.domain;
// Generated Mar 21, 2019 2:14:44 PM by Hibernate Tools 5.1.10.Final

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * User generated by hbm2java
 */
@Entity
@Table(name = "user", catalog = "painmanagement", uniqueConstraints = { @UniqueConstraint(columnNames = "Email"),
		@UniqueConstraint(columnNames = "IcNumber") })
public class User implements java.io.Serializable {

	private Integer userId;
	private String email;
	private String icNumber;
	private String name;
	private int roleId;
	private String password;
	private String profilePictureUri;
	private String phoneNumber;
	private Set<Userpatient> userpatients = new HashSet<Userpatient>(0);

	public User() {
	}

	public User(String email, String icNumber, String name, int roleId) {
		this.email = email;
		this.icNumber = icNumber;
		this.name = name;
		this.roleId = roleId;
	}

	public User(String email, String icNumber, String name, int roleId, String password, String profilePictureUri,
			String phoneNumber, Set<Userpatient> userpatients) {
		this.email = email;
		this.icNumber = icNumber;
		this.name = name;
		this.roleId = roleId;
		this.password = password;
		this.profilePictureUri = profilePictureUri;
		this.phoneNumber = phoneNumber;
		this.userpatients = userpatients;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "UserId", unique = true, nullable = false)
	public Integer getUserId() {
		return this.userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	@Column(name = "Email", unique = true, nullable = false, length = 320)
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "IcNumber", unique = true, nullable = false, length = 45)
	public String getIcNumber() {
		return this.icNumber;
	}

	public void setIcNumber(String icNumber) {
		this.icNumber = icNumber;
	}

	@Column(name = "Name", nullable = false, length = 45)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "RoleId", nullable = false)
	public int getRoleId() {
		return this.roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	@Column(name = "Password", length = 128)
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name = "ProfilePictureUri")
	public String getProfilePictureUri() {
		return this.profilePictureUri;
	}

	public void setProfilePictureUri(String profilePictureUri) {
		this.profilePictureUri = profilePictureUri;
	}

	@Column(name = "PhoneNumber", length = 45)
	public String getPhoneNumber() {
		return this.phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
	public Set<Userpatient> getUserpatients() {
		return this.userpatients;
	}

	public void setUserpatients(Set<Userpatient> userpatients) {
		this.userpatients = userpatients;
	}

}
